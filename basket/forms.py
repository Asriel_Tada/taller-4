from django import forms
from .models import *

class CoachForm(forms.ModelForm):
    class Meta:
        model = Coach
        fields = '__all__'

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = '__all__'

class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = '__all__'

CHOICES = (('Choach','Choach'),('Player','Player'))


class registroForm(forms.Form):
    username=forms.CharField( label='Nombre de usuario',required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'username' , 'placeholder':'Nombre de usuario'} ))
    first_name=forms.CharField( label='Nombres',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'first_name' , 'placeholder':'Nombres'} ))
    last_name=forms.CharField(label='Apellidos', required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'last_name' , 'placeholder':'Apellidos'} ))
    rut=forms.CharField(label='Rut',  required=True , widget=forms.TextInput ( attrs={'class':'form-control' , 'id':'run' , 'placeholder':'12345678-9'} ))
    email=forms.EmailField( required=True , widget = forms.EmailInput ( attrs = {'class':'form-control' , 'id':'email' , 'placeholder':'Usuario@server.com'} ))
    age = forms.IntegerField()
    typo=forms.ChoiceField(label='Tipo de usuario', choices=CHOICES)
    password=forms.CharField( label='Contraseña', required=True , min_length=8 , widget=forms.PasswordInput ( attrs= {'class':'form-control' , 'id':'password' , 'placeholder':'Contraseña'} ))
    password2 =forms.CharField( label='Confirmar Contraseña',required=True , min_length=8 , widget=forms.PasswordInput ( attrs= {'class':'form-control', 'placeholder':'Confirma Contraseña'} ))

    def clean_username(self):
        username = self.cleaned_data.get('username')

        if User.objects.filter(username = username ).exists():
            raise forms.ValidationError('El nombre de usuario  ya se encuentra en uso')

        return username 
    
    def clean_email(self):
        email = self.cleaned_data.get('email')

        if User.objects.filter(email = email ).exists():
            raise forms.ValidationError('El email ya se encuentra en uso')
        
        return email 

    def clean(self):
        cleaned_data = super().clean()

        if cleaned_data.get('password2') != cleaned_data.get('password'): 
            self.add_error('password2','La contraseña no coincide')

    def save(self):
        user = User.objects.create_user(
            self.cleaned_data.get('username'),
            self.cleaned_data.get('email'),
            self.cleaned_data.get('password'),
            )
        print (self.cleaned_data.get('last_name'))
        ruti= self.cleaned_data.get('rut')

        user.last_name=self.cleaned_data.get('last_name')
        user.first_name=self.cleaned_data.get('first_name')
        name = user.first_name +" " + user.last_name 
        age = self.cleaned_data.get("age")
        
        tipo = self.cleaned_data.get('typo')
        if tipo == 'Player':
            user.groups.add(2)
        elif tipo == 'Choach':
            user.groups.add(1)
        pro = Coach(user=user, name =name , rut=ruti, age = age, email = self.cleaned_data.get('email'), nickname =  self.cleaned_data.get('username'))
        pro.save()
        user.save()
        
        return user
